//Logical Operator

void main() {
  int x = 10;
  int y = 8;

  print(x && y);
  print(x || y);
  print(!x);
  print(!y);

  //Error: A value of type 'int' can't be assigned to a variable of type 'bool'.
}
