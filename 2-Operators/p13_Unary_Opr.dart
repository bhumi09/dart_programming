//Unary Operators
//Pre increment and Post increment

void main() {
  int x = 10;

  print(++x);
  print(x++);
  print(x);
}
