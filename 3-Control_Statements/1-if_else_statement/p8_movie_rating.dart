//Example - Movie Rating
void main() {
  double imdbRating = 5.5;

  if (imdbRating >= 9.0)
    print("Superhit");
  else if (imdbRating >= 8.5)
    print("Average");
  else
    print("Not Good");
}
