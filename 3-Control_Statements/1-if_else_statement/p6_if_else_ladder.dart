//if_else_ladder
void main() {
  int number = 7;

  if (number > 0)
    print("Number is Positive");
  else if (number < 0)
    print("Number is Negative");
  else
    print("Zero");
}
