// Taking integer values from user

import 'dart:io';

void main() {
  String? name = stdin.readLineSync();
  print("Name :: $name");

  //int? age = int.parse(stdin.readLineSync());
  //Error: The argument type 'String?' can't be assigned to the parameter type 'String'
  //because 'String?' is nullable and 'String' isn't.

  int? age = int.parse(stdin.readLineSync()!);
  print("Age :: $age");
}
