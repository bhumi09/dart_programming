void fun(int x) {
  if (x > 10) return;

  print(x);
  fun(x + 1);
}

void main() {
  fun(1);
}
