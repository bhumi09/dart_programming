void fun(String name, double sal) {
  print("In Fun");
  print(name);
  print(sal);
}

void main() {
  print("Start Main");
  fun(20.5, "Kanha");
  fun(sal: 20.5, name: "Kanha");
  print("End Main");
}
