
void fun(String name, double sal) {

  print("In Fun");
  print(name);
  print(sal);

}

void main() {

  print("Start main");
  fun(10.5, "Kanha");   // Error: The argument type 'double' can't be assigned to the parameter type 'String'.
                        // Error: The argument type 'String' can't be assigned to the parameter type 'double'.
  print("End Main"); 
}