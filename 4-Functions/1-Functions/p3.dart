
void fun() {

  print("In Fun");
}

void main() {

  print("Start Main");
  fun(10);              // Error: Too many positional arguments: 0 allowed, but 1 found.
  print("End Main");
  
}