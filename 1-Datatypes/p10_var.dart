//Datatype - var

void main() {
  var x = "Bhumika";
  print(x);

  x = 20.5; //Error: A value of type 'double' can't be assigned to a variable of type 'String'.
  print(x);

  x = true; //Error: A value of type 'bool' can't be assigned to a variable of type 'String'.
  print(x);
}
